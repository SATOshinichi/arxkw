// Monocypher version 3.0.0
//
// SPDX-License-Identifier: CC0-1.0
//
// Written in 2017-2020 by Loup Vaillant
//
// To the extent possible under law, the author(s) have dedicated all copyright
// and related neighboring rights to this software to the public domain
// worldwide.  This software is distributed without any warranty.
//
// You should have received a copy of the CC0 Public Domain Dedication along
// with this software.  If not, see
// <https://creativecommons.org/publicdomain/zero/1.0/>


#include <stdint.h>
#include <stddef.h>

/////////////////
/// Utilities ///
/////////////////
#define FOR_T(type, i, start, end) for (type i = (start); i < (end); i++)
#define FOR(i, start, end)         FOR_T(size_t, i, start, end)
// speed
#define WIPE_CTX(ctx)              do {} while (0)
#define WIPE_BUFFER(buffer)        do {} while (0)

#define MIN(a, b)                  ((a) <= (b) ? (a) : (b))
#define MAX(a, b)                  ((a) >= (b) ? (a) : (b))
#define ALIGN(x, block_size)       ((~(x) + 1) & ((block_size) - 1))
typedef int8_t   i8;
typedef uint8_t  u8;
typedef int16_t  i16;
typedef uint32_t u32;
typedef int32_t  i32;
typedef int64_t  i64;
typedef uint64_t u64;

static const u8 zero[128] = {0};

static u32 load24_le(const u8 s[3])
{
    return (u32)s[0]
        | ((u32)s[1] <<  8)
        | ((u32)s[2] << 16);
}

static u32 load32_le(const u8 s[4])
{
    return (u32)s[0]
        | ((u32)s[1] <<  8)
        | ((u32)s[2] << 16)
        | ((u32)s[3] << 24);
}

static u64 load64_le(const u8 s[8])
{
    return load32_le(s) | ((u64)load32_le(s+4) << 32);
}

static void store32_le(u8 out[4], u32 in)
{
    out[0] =  in        & 0xff;
    out[1] = (in >>  8) & 0xff;
    out[2] = (in >> 16) & 0xff;
    out[3] = (in >> 24) & 0xff;
}

static void store64_le(u8 out[8], u64 in)
{
    store32_le(out    , (u32)in );
    store32_le(out + 4, in >> 32);
}

static u64 rotr64(u64 x, u64 n) { return (x >> n) ^ (x << (64 - n)); }
static u32 rotl32(u32 x, u32 n) { return (x << n) ^ (x >> (32 - n)); }

static int neq0(u64 diff)
{   // constant time comparison to zero
    // return diff != 0 ? -1 : 0
    u64 half = (diff >> 32) | ((u32)diff);
    return (1 & ((half - 1) >> 32)) - 1;
}

static u64 x16(const u8 a[16], const u8 b[16])
{
    return (load64_le(a + 0) ^ load64_le(b + 0))
        |  (load64_le(a + 8) ^ load64_le(b + 8));
}
static u64 x32(const u8 a[32],const u8 b[32]){return x16(a,b)| x16(a+16, b+16);}
static u64 x64(const u8 a[64],const u8 b[64]){return x32(a,b)| x32(a+32, b+32);}
int crypto_verify16(const u8 a[16], const u8 b[16]){ return neq0(x16(a, b)); }
int crypto_verify32(const u8 a[32], const u8 b[32]){ return neq0(x32(a, b)); }
int crypto_verify64(const u8 a[64], const u8 b[64]){ return neq0(x64(a, b)); }

////////////////
/// Chacha 8 ///
////////////////
#define QUARTERROUND(a, b, c, d)     \
    a += b;  d = rotl32(d ^ a, 16);  \
    c += d;  b = rotl32(b ^ c, 12);  \
    a += b;  d = rotl32(d ^ a,  8);  \
    c += d;  b = rotl32(b ^ c,  7)

static void chacha8_rounds(u32 out[16], const u32 in[16])
{
    u32 t0  = in[ 0];  u32 t1  = in[ 1];  u32 t2  = in[ 2];  u32 t3  = in[ 3];
    u32 t4  = in[ 4];  u32 t5  = in[ 5];  u32 t6  = in[ 6];  u32 t7  = in[ 7];
    u32 t8  = in[ 8];  u32 t9  = in[ 9];  u32 t10 = in[10];  u32 t11 = in[11];
    u32 t12 = in[12];  u32 t13 = in[13];  u32 t14 = in[14];  u32 t15 = in[15];

    FOR (i, 0, 4) { // 8 rounds, 2 rounds per loop.
        QUARTERROUND(t0, t4, t8 , t12); // column 0
        QUARTERROUND(t1, t5, t9 , t13); // column 1
        QUARTERROUND(t2, t6, t10, t14); // column 2
        QUARTERROUND(t3, t7, t11, t15); // column 3
        QUARTERROUND(t0, t5, t10, t15); // diagonal 0
        QUARTERROUND(t1, t6, t11, t12); // diagonal 1
        QUARTERROUND(t2, t7, t8 , t13); // diagonal 2
        QUARTERROUND(t3, t4, t9 , t14); // diagonal 3
    }
    out[ 0] = t0;   out[ 1] = t1;   out[ 2] = t2;   out[ 3] = t3;
    out[ 4] = t4;   out[ 5] = t5;   out[ 6] = t6;   out[ 7] = t7;
    out[ 8] = t8;   out[ 9] = t9;   out[10] = t10;  out[11] = t11;
    out[12] = t12;  out[13] = t13;  out[14] = t14;  out[15] = t15;
}

static void chacha8_init_key(u32 block[16], const u8 key[32])
{
    // constant
    block[0] = load32_le((const u8*)"expa");
    block[1] = load32_le((const u8*)"nd 3");
    block[2] = load32_le((const u8*)"2-by");
    block[3] = load32_le((const u8*)"te k");
    // key
    FOR (i, 0, 8) {
        block[i+4] = load32_le(key + i*4);
    }
}

static u64 chacha8_core(u32 input[16], u8 *cipher_text, const u8 *plain_text,
                         size_t text_size)
{
    // Whole blocks
    u32    pool[16];
    size_t nb_blocks = text_size >> 6;
    FOR (i, 0, nb_blocks) {
        chacha8_rounds(pool, input);
        if (plain_text != 0) {
            FOR (j, 0, 16) {
                u32 p = pool[j] + input[j];
                store32_le(cipher_text, p ^ load32_le(plain_text));
                cipher_text += 4;
                plain_text  += 4;
            }
        } else {
            FOR (j, 0, 16) {
                u32 p = pool[j] + input[j];
                store32_le(cipher_text, p);
                cipher_text += 4;
            }
        }
        input[12]++;
        if (input[12] == 0) {
            input[13]++;
        }
    }
    text_size &= 63;

    // Last (incomplete) block
    if (text_size > 0) {
        if (plain_text == 0) {
            plain_text = zero;
        }
        chacha8_rounds(pool, input);
        u8 tmp[64];
        FOR (i, 0, 16) {
            store32_le(tmp + i*4, pool[i] + input[i]);
        }
        FOR (i, 0, text_size) {
            cipher_text[i] = tmp[i] ^ plain_text[i];
        }
        WIPE_BUFFER(tmp);
    }
    WIPE_BUFFER(pool);
    return input[12] + ((u64)input[13] << 32) + (text_size > 0);
}

void crypto_hchacha8(u8 out[32], const u8 key[32], const u8 in [16])
{
    u32 block[16];
    chacha8_init_key(block, key);
    // input
    FOR (i, 0, 4) {
        block[i+12] = load32_le(in + i*4);
    }
    chacha8_rounds(block, block);
    // prevents reversal of the rounds by revealing only half of the buffer.
    FOR (i, 0, 4) {
        store32_le(out      + i*4, block[i     ]); // constant
        store32_le(out + 16 + i*4, block[i + 12]); // counter and nonce
    }
    WIPE_BUFFER(block);
}

void crypto_chacha8_lqb(u8 *cipher_text, const u8 *plain_text,
                        size_t text_size, const u8 key[32], const u8 lqb[16])
{
    u32 input[16];
    chacha8_init_key(input, key);
    input[12] = load32_le(lqb);
    input[13] = load32_le(lqb + 4);
    input[14] = load32_le(lqb + 8);
    input[15] = load32_le(lqb + 12);
    (void)chacha8_core(input, cipher_text, plain_text, text_size);
    WIPE_BUFFER(input);
}

u64 crypto_chacha8_ctr(u8 *cipher_text, const u8 *plain_text,
                        size_t text_size, const u8 key[32], const u8 nonce[8],
                        u64 ctr)
{
    u32 input[16];
    chacha8_init_key(input, key);
    input[12] = (u32) ctr;
    input[13] = (u32)(ctr >> 32);
    input[14] = load32_le(nonce);
    input[15] = load32_le(nonce + 4);
    ctr = chacha8_core(input, cipher_text, plain_text, text_size);
    WIPE_BUFFER(input);
    return ctr;
}

void crypto_chacha8_zerostream(uint8_t *stream, size_t text_size, const uint8_t key[32]) {
    u32 input[16];
    chacha8_init_key(input, key);
    input[12] = 0; input[13] = 0; input[14] = 0; input[15] = 0;
    (void)chacha8_core(input, stream, 0, text_size);
}

u64 crypto_xchacha8_ctr(u8 *cipher_text, const u8 *plain_text,
                         size_t text_size,
                         const u8 key[32], const u8 nonce[24], u64 ctr)
{
    u8 sub_key[32];
    crypto_hchacha8(sub_key, key, nonce);
    ctr = crypto_chacha8_ctr(cipher_text, plain_text, text_size,
                              sub_key, nonce+16, ctr);
    WIPE_BUFFER(sub_key);
    return ctr;
}

void crypto_xchacha8(u8 *cipher_text, const u8 *plain_text, size_t text_size,
                      const u8 key[32], const u8 nonce[24])
{
    crypto_xchacha8_ctr(cipher_text, plain_text, text_size, key, nonce, 0);
}


/*
 * vectors.c - Reference implementation of ARX-KW to generate test vectors
 *
 * Written in 2020 by AUTHOR NAME
 *
 * To the extent possible under law, the author(s) have dedicated all copyright
 * and related and neighboring rights to this software to the public domain
 * worldwide. This software is distributed without any warranty.
 *
 * You should have received a copy of the CC0 Public Domain Dedication along
 * with this software. If not, see
 * <https://creativecommons.org/publicdomain/zero/1.0/>.
 */
#include <assert.h>
#include <inttypes.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

extern int siphash(const uint8_t *in, const size_t inlen, const uint8_t *k,
                   uint8_t *out, const size_t outlen);
extern void crypto_chacha8_lqb(uint8_t *cipher_text, const uint8_t *plain_text,
                               size_t text_size, const uint8_t key[32],
                               const uint8_t lqb[16]);
extern void crypto_chacha8_zerostream(uint8_t *stream, size_t text_size,
                                      const uint8_t key[32]);
extern void crypto_xchacha8(uint8_t *cipher_text, const uint8_t *plain_text,
                            size_t text_size, const uint8_t key[32],
                            const uint8_t nonce[24]);
extern int crypto_verify16(const uint8_t a[16], const uint8_t b[16]);

static void hexdump(const char *prefix, uint8_t d[], size_t len) {
  printf("%s: ", prefix);
  for (size_t i = 0; i < len; ++i) {
    printf("%02x", d[i]);
    if (i > 0 && i % 27 == 0)
      fputs("\n   ", stdout);
  }
  puts("");
}

static void arxkw_8_2_4_e_encrypt(uint8_t C[], uint8_t T[static 16],
                                  const uint8_t K[static 48], const uint8_t P[],
                                  size_t Plen) {
  const uint8_t *K1 = K;
  const uint8_t *K2 = K + 16;

  assert(Plen <= 64);
  (void)siphash(P, Plen, K1, T, 16);
  crypto_chacha8_lqb(C, P, Plen, K2, T);
}

static bool arxkw_8_2_4_e_decrypt(uint8_t P[], const uint8_t T[static 16],
                                  const uint8_t K[static 48], const uint8_t C[],
                                  size_t Clen) {
  const uint8_t *K1 = K;
  const uint8_t *K2 = K + 16;
  uint8_t T_[16];

  assert(Clen <= 64);
  crypto_chacha8_lqb(P, C, Clen, K2, T);
  (void)siphash(P, Clen, K1, T_, 16);
  return (crypto_verify16(T, T_) == 0);
}

static void arxkw_8_2_4_g_encrypt(uint8_t C[], uint8_t T[static 16],
                                  const uint8_t K[static 32], const uint8_t P[],
                                  size_t Plen) {
  uint8_t G[48];
  const uint8_t *K1 = G;
  const uint8_t *K2 = G + 16;

  assert(Plen <= 64);
  crypto_chacha8_zerostream(G, sizeof(G), K);
  (void)siphash(P, Plen, K1, T, 16);
  crypto_chacha8_lqb(C, P, Plen, K2, T);
}

static bool arxkw_8_2_4_g_decrypt(uint8_t P[], const uint8_t T[static 16],
                                  const uint8_t K[static 32], const uint8_t C[],
                                  size_t Clen) {
  uint8_t G[48];
  const uint8_t *K1 = G;
  const uint8_t *K2 = G + 16;
  uint8_t T_[16];

  assert(Clen <= 64);
  crypto_chacha8_zerostream(G, sizeof(G), K);
  crypto_chacha8_lqb(P, C, Clen, K2, T);
  (void)siphash(P, Clen, K1, T_, 16);
  return (crypto_verify16(T, T_) == 0);
}

static void arxkw_8_2_4_ex_encrypt(uint8_t C[], uint8_t T[static 16],
                                   const uint8_t K[static 48],
                                   const uint8_t P[], size_t Plen) {
  const uint8_t *K1 = K;
  const uint8_t *K2 = K + 16;
  uint8_t N[24] = "arbitrEX";

  (void)siphash(P, Plen, K1, T, 16);
  memcpy(N + 8, T, 16);
  hexdump("N", N, sizeof(N));
  crypto_xchacha8(C, P, Plen, K2, N);
}

static bool arxkw_8_2_4_ex_decrypt(uint8_t P[], const uint8_t T[static 16],
                                   const uint8_t K[static 48],
                                   const uint8_t C[], size_t Clen) {
  const uint8_t *K1 = K;
  const uint8_t *K2 = K + 16;
  uint8_t T_[16];
  uint8_t N[24] = "arbitrEX";

  memcpy(N + 8, T, 16);
  hexdump("N", N, sizeof(N));
  crypto_xchacha8(P, C, Clen, K2, N);
  (void)siphash(P, Clen, K1, T_, 16);
  return (crypto_verify16(T, T_) == 0);
}

static void arxkw_8_2_4_gx_encrypt(uint8_t C[], uint8_t T[static 16],
                                   const uint8_t K[static 32],
                                   const uint8_t P[], size_t Plen) {
  uint8_t G[48];
  const uint8_t *K1 = G;
  const uint8_t *K2 = G + 16;
  uint8_t N[24] = "arbitrGX";

  crypto_chacha8_zerostream(G, sizeof(G), K);
  (void)siphash(P, Plen, K1, T, 16);
  memcpy(N + 8, T, 16);
  hexdump("N", N, sizeof(N));
  crypto_xchacha8(C, P, Plen, K2, N);
}

static bool arxkw_8_2_4_gx_decrypt(uint8_t P[], const uint8_t T[static 16],
                                   const uint8_t K[static 32],
                                   const uint8_t C[], size_t Clen) {
  uint8_t G[48];
  const uint8_t *K1 = G;
  const uint8_t *K2 = G + 16;
  uint8_t T_[16];
  uint8_t N[24] = "arbitrGX";

  crypto_chacha8_zerostream(G, sizeof(G), K);
  memcpy(N + 8, T, 16);
  hexdump("N", N, sizeof(N));
  crypto_xchacha8(P, C, Clen, K2, N);
  (void)siphash(P, Clen, K1, T_, 16);
  return (crypto_verify16(T, T_) == 0);
}

int main(void) {
  uint8_t K[48];
  uint8_t P[32] = {
      0xde, 0xad, 0xbe, 0xef, 0xde, 0xad, 0xbe, 0xef, 0xde, 0xad, 0xbe,
      0xef, 0xde, 0xad, 0xbe, 0xef, 0xde, 0xad, 0xbe, 0xef, 0xde, 0xad,
      0xbe, 0xef, 0xde, 0xad, 0xbe, 0xef, 0xde, 0xad, 0xbe, 0xef,
  };
  uint8_t C[32];
  uint8_t P_[32];
  uint8_t T[16];
  int64_t diff_e, diff_d;

  for (uint8_t i = 0; i < sizeof(K); ++i)
    K[i] = i;

  puts("\\subsection{ARX-KW-8-2-4-E}\n\\begin{verbatim}");
  hexdump("K", K, 384 / 8);
  hexdump("P", P, sizeof(P));
  arxkw_8_2_4_e_encrypt(C, T, K, P, sizeof(P));
  hexdump("T", T, sizeof(T));
  hexdump("C", C, sizeof(C));
  if (!arxkw_8_2_4_e_decrypt(P_, T, K, C, sizeof(C))) {
    fprintf(stderr, "bad decrypt\n");
    return 1;
  }
  puts("\\end{verbatim}\n\n\\subsection{ARX-KW-8-2-4-G}\n\\begin{verbatim}");
  hexdump("K", K, 256 / 8);
  hexdump("P", P, sizeof(P));
  arxkw_8_2_4_g_encrypt(C, T, K, P, sizeof(P));
  hexdump("T", T, sizeof(T));
  hexdump("C", C, sizeof(C));
  if (!arxkw_8_2_4_g_decrypt(P_, T, K, C, sizeof(C))) {
    fprintf(stderr, "bad decrypt\n");
    return 1;
  }

  puts("\\end{verbatim}\n\n\\subsection{ARX-KW-8-2-4-EX}\n\\begin{verbatim}");
  hexdump("K", K, 384 / 8);
  hexdump("P", P, sizeof(P));
  arxkw_8_2_4_ex_encrypt(C, T, K, P, sizeof(P));
  hexdump("T", T, sizeof(T));
  hexdump("C", C, sizeof(C));
  if (!arxkw_8_2_4_ex_decrypt(P_, T, K, C, sizeof(C))) {
    fprintf(stderr, "bad decrypt\n");
    return 1;
  }
  puts("\\end{verbatim}\n\n\\subsection{ARX-KW-8-2-4-GX}\n\\begin{verbatim}");
  hexdump("K", K, 256 / 8);
  hexdump("P", P, sizeof(P));
  arxkw_8_2_4_gx_encrypt(C, T, K, P, sizeof(P));
  hexdump("T", T, sizeof(T));
  hexdump("C", C, sizeof(C));
  if (!arxkw_8_2_4_gx_decrypt(P_, T, K, C, sizeof(C))) {
    fprintf(stderr, "bad decrypt\n");
    return 1;
  }
  puts("\\end{verbatim}");

  return 0;
}
